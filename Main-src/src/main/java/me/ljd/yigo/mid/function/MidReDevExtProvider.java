package me.ljd.yigo.mid.function;

import com.bokesoft.yigo.parser.IFunImplCluster;
import com.bokesoft.yigo.parser.IFunctionProvider;

import me.ljd.yigo.utils.DBManagerUtil2;

public class MidReDevExtProvider implements IFunctionProvider {

	public IFunImplCluster[] getClusters() {
		DBManagerUtil2.init();
		return new IFunImplCluster[]{new MidReDevExt()};
	}


}
